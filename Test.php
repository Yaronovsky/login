<?php
namespace PROJ;

class Test
{
    public $testVar;

    public function __construct($var)
    {
        $this->testVar = $var;
    }

    public function show() 
    {
        echo $this->testVar;
    }
}