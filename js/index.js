window.addEventListener('load', () => {
    const submit = document.querySelector('.Login button[type="submit"]');
        submit.addEventListener('click', async () => {
            const url = '/login/backend/auth.php/';
            const mainForm = document.querySelector('[name="mainForm"]');
            let requestData = Array.from(mainForm)
            .reduce((accumualtor, input) => {
                accumualtor[input.name] = input.value;
                return accumualtor;
            }, {});
            requestData.action = location.hash.substr(1);
            let str = JSON.stringify({'mainForm': requestData});
            let response = await fetch(url, {
                method: 'POST',
                body: str
            });
            response = await response.json();
            if (!response.register) {
                const login = document.querySelector('.Login');
                    login.classList.add('Login--error');
                    setTimeout(() => {
                        login.classList.remove('Login--error');
                        mainForm.reset();
                    }, 3000)
            }
            else if (response.register) {
                const login = document.querySelector('.Login');
                    login.classList.add('Login--success');
                setTimeout(() => {
                    login.classList.remove('Login--success');
                    mainForm.reset();
                }, 3000)
            }
        })
});