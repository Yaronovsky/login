<?php
require_once '../vendor/autoload.php';
require_once 'config.php';

use BACK\Login\{
    AuthDataType,
    AuthGateway,
    AuthValidator,
    Auth
}; 

$formData = file_get_contents('php://input');
$formDataArray = json_decode($formData, true);
$formData = filter_var_array($formDataArray['mainForm'], FILTER_SANITIZE_STRING);
$dataType = new AuthDataType($formData);

$connection = new PDO(
    DSN, 
    MYSQL_USER, 
    MYSQL_PASSWORD
);

$validator = new AuthValidator();

$gateway = new AuthGateway(
    $dataType, 
    $connection,
    $validator
);

$auth = new Auth($gateway);
if($auth->shouldRegister()) {
    echo $auth->register();
    die();
};

echo json_encode([$gateway->getProperty('action') => false]);
