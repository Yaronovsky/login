<?php
namespace BACK\Login;

class Auth
{
    private $gateway;

    public function __construct(AuthGatewayInterface $gateway)
    {
        $this->gateway = $gateway;    
    }

    public function register()
    {
        $action = $this->gateway->getProperty('action');
        return $this->gateway->register() ? 
            json_encode([$action => true]) : 
            json_encode([$action => false]);
    }

    public function login() 
    {
        $this->gateway->login();
    }

    public function shouldRegister()
    {
        return $this->gateway->shouldRegister();
    }
}