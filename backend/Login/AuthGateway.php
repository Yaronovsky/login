<?php
namespace BACK\Login;

class AuthGateway implements AuthGatewayInterface
{
    private $connection;
    private $dataType;
    private $validator;

    public function __construct(
        AuthDataType $dataType, 
        \PDO $connection,
        AuthValidator $validator
        )
    {
        $this->dataType = $dataType;
        $this->connection = $connection;
        $this->validator = $validator;
    }

    public function login() 
    {

    }

    public function register()
    {
        $query = 'INSERT INTO `user_details` (id, email, user_name, pass) 
            VALUES(null, :email, :user_name, :pass)';
        $stmt = $this->connection->prepare($query);
        $status = $stmt->execute([
            ':email' => $this->dataType->email,
            ':user_name' => $this->dataType->user,
            ':pass' => $this->dataType->password,
        ]);
        return $status;
    }

    public function shouldLogin()
    {

    }

    public function shouldRegister()
    {
        $query = 'SELECT 1 FROM `user_details` 
                    WHERE 
                        email = :email OR 
                        user_name = :user_name';
                
        $stmt = $this->connection->prepare($query);
        $stmt->execute([
            ':email' => $this->dataType->email,
            ':user_name' => $this->dataType->user,
        ]);
        return  $stmt->fetch() ? false : true;
    }

    public function getProperty($propertyName)
    {
        return $this->dataType->$propertyName;
    }
}