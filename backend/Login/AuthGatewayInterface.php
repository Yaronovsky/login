<?php
namespace BACK\Login;

interface AuthGatewayInterface
{
    public function login();
    public function register();
}